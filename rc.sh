#!/bin/sh
#   Copyright 2012 Gary Wright http://identi.ca/wrightsolutions
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

#   This is a partner script to Python rc.py
#   rc.py gives a rowcount TOTAL for all tables in a sqlite database
#   This shell script is intended as a modified 'wc -l' and will
#   Substitute the rowcount (rather than linecount) if the file being
#   examined is a sqlite database

#   The usual 'total' line that is displayed by 'wc -l' is removed
#   from the output

#   Ensure rc.sh and rc.py live in the same directory and then try:
#   ./rc.sh *.*
#   If there are sqlite databases in the current directory (and found
#   by *.*) then you should see row counts rather than line counts for
#   those databases.
#   chmod 755 ./rc.* to overcome permission complaints

DIRNAME_=$(dirname $0)
#CMDNAME_=$(basename $0)
#printf "$DIRNAME_\n"
#printf "$CMDNAME_\n"
CMDBIN_='/usr/bin/wc -l'	# word count (lines)
#printf "$CMDBIN_\n"
#PYNAME_="${DIRNAME_}/argument-count.py"
PYNAME_="${DIRNAME_}/rc.py"
#PYNAME_="${DIRNAME_}"
#AWKARGS_="/\.sqlite$/ {cmd ${PYNAME_} \$2 | getline var; print var}"
AWKARGS_="/\.sqlite$/ {print \$2}"
#AWKARGS_="/\.sqlite$/ {cmd = \"${PYNAME_} \$2\"; print cmd}"
AWKARGS_="/\.sqlite$/ {cmd = \"${PYNAME_} \"\$2; cmd | getline var; print var;} \
\$0 !~ /\.sqlite/ && \$0 !~ /total\$/ { print \$0 }"
#printf "${AWKARGS_}\n"
${CMDBIN_} $* | awk "${AWKARGS_}"
