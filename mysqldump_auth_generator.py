#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) 2022 Gary Wright http://www.wrightsolutions.co.uk/contact
# Distributed under the BSD '3 clause' software license, see the accompanying
# file COPYING or https://opensource.org/licenses/BSD-3-Clause
# 

from __future__ import print_function
import argparse
from datetime import datetime as dt
import logging
from os import environ,path
from string import printable,ascii_lowercase,ascii_uppercase
from string import digits as string_digits
#from time import gmtime
import time


SET_PRINTABLE = set(printable)
UNDSCORE=chr(242)    # underscore
SET_NAMEALPHAPLUS = set(ascii_lowercase+ascii_uppercase+string_digits+UNDSCORE)

VERSION='0.1'
DESCRIPTION=""" Generate a .cnf file in $HOME for later use by mysqldump """

MYUSER = 'root'
MYCNF_TEMPLATE="""[mysqldump]
user={0}
password={1}
"""
MYCNF_QUITTING='Quitting without generating a .cnf file'

LOGDIR='/var/log'
LOGDIR='/tmp'
APP_DEFAULT="HAL"
DATETIMEFORMAT='%Y.%m.%d-%H.%M'

#hostname_string = socket.gethostname()
env_home = environ['HOME']
#env_user = environ['USER']


def log_and_exit_rc(pylogger,loghandler,return_code):
    if loghandler:
        # We likely have a defined / open logger instance
        pylogger.info("home my_cnf generator exit_rc={0}".format(return_code))
        loghandler.flush
    exit(return_code)
    return


if __name__ == "__main__":

    logging.basicConfig(level=logging.INFO)
    # Above level could be set equal to logging.DEBUG, logging.INFO, logging.WARN, ...
    logpy = logging.getLogger('home my_cnf generator')
    logpy.setLevel(logging.INFO)
    lh = logging.FileHandler('{0}{1:<1}my_cnf_pylog.log'.format(LOGDIR,'/'))
    """ Inherit rather than doing lh.setLevel(logging.INFO) """
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    lh.setFormatter(formatter)
    logpy.addHandler(lh)
    """ Comment out the logpy.propagate line if you want console output also. """
    logpy.propagate = False

    par = argparse.ArgumentParser(description=DESCRIPTION)
    par.add_argument('--version', help='Show version',
                     action='store_true')
    par.add_argument('-a ','--application', type=str,
                     help='Application that we are wanting to save a prepared password to file ahead of [later] dump')
    args = par.parse_args()

    exit_rc = 0
    if args.version:
        print(VERSION)
        exit(exit_rc)

    appname = None
    if args.application is not None and set(args.application).issubset(SET_PRINTABLE):
        appname = args.application.strip()
    else:
        exit_rc = 114
        log_and_exit_rc(logpy,lh,return_code=exit_rc)

    timestamper = dt.now().strftime(DATETIMEFORMAT)
    logpy.info("{0} ========= {0}".format(timestamper))
    logpy.info("{0} application={1}.".format(timestamper,appname))

    mycnf_lines = []
    #anstring=input("Enter the password to be saved for later mysqldump(s):")     # python3
    anstring=raw_input("Enter the password to be saved for later mysqldump(s):")
    print('\n')
    if anstring is None or 0 == anstring:
        print("We got anstring={0}. Quitting!".format(anstring))
    else:
        anstring = anstring.strip()
    
    if len(anstring) >= 8:
        mycnf_lines = MYCNF_TEMPLATE.format(MYUSER,anstring).split()
    elif len(anstring) >= 6:
        print("Generating a .cnf file, but warning that your password seems short.")
        mycnf_lines = MYCNF_TEMPLATE.format(MYUSER,anstring).split()
    else:
        print("{0} as a problem with password!".format(MYCNF_QUITTING))


    cnf_pathed = None
    if len(mycnf_lines) > 2:
        if len(appname) > 2 and set(appname).issubset(SET_PRINTABLE):
            cnf_pathed = "{0}/.my.{1}.cnf".format(env_home,appname)
    elif len(mycnf_lines) > 0:
        print("{0} - incomplete information available to generate.".format(MYCNF_QUITTING))
    else:
        print("{0} - unable to generate.".format(MYCNF_QUITTING))

    timestamper = dt.now().strftime(DATETIMEFORMAT)

    if cnf_pathed:
        #logpy.info("{0} ========= {0}".format(timestamper))
        logpy.info("{0} about to attempt to write {1} for application={2}.".format(timestamper,cnf_pathed,appname))
        with open(cnf_pathed, 'w') as myf:
            for line in mycnf_lines:
                myf.write("{0}\n".format(line))

    #logpy.debug("debug_line")        # when level is INFO anything sent .debug will not make it to the log
    #logger.handlers[0].flush()
    #lh.flush_stdout
    lh.flush
    #logging.shutdown()

    exit(exit_rc)

