#!/usr/bin/env python
#   Copyright 2012 Gary Wright http://identi.ca/wrightsolutions
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

#   License exemptions granted to the official Python source and binary
#   and official Sqlite source and binary, both of whom are
#   granted rights to bundle this source with their own under
#   licenses compatible with their normal distribution policy.

#    ~/rc.py *.*   would see processing handled by    for arg_index in range()
#    ls *.sqlite | ~/rc.py   would be handled by function process_from_stdin()

#   Initially tested using Python 2.7.3 on GNU/Linux
#   Python 2.6.6 compatibility changes included:
#	o Indexing of {} so {} becomes {0} and so on
#	o Commenting out of 'finally' blocks which are well
#	  supported in 2.7 but I avoid in 2.6

from sys import argv,exit
import os.path
from os import stat
import sqlite3 as db3
import logging
from string import punctuation


def isfile_and_positive_size(target_path_given):
	target_path = target_path_given.strip()
	if not os.path.isfile(target_path):
		return False
	if not os.stat(target_path).st_size:
		return False
	return True


def rowcount_report(rowcount,target_path):
	""" print 'rowcount is {0:>6} for {1}'.format(rowcount,target_path) """
	""" logpy.info('rowcount is %s for %s',rowcount,target_path) """
	""" Prefer the .format way as want right justification so below """
	logpy.info('rowcount is {0:>6} for {1}'.format(rowcount,target_path))

	if (rowcount > 0):
		print('{0:>6} {1}'.format(rowcount, target_path))
	else:
		print '     0 {0}'.format(target_path)
	return


def connect_get_cursor(database_path):
	global con
	cur = None
	try:
		""" Ideally the next would be READONLY but we are using Python
		standard DB-API so fit with what it offers. """
		con = db3.connect(database_path,timeout=2)		
	except db3.Error, e:
		logpy.error('Sqlite connection error occurred: %s', e.args[0])
		return None
	except:
		logpy.error('Sqlite connection error.')
		return None
	else:
		con.isolation_level = None		
		cur = con.cursor()
	return cur


def compound_rowcount(database_path):
	""" Design decision here is to return 0, rather than 'continue' when a
	Database error is encountered. Rationale is that if there were 3 tables
	each of 10 rows, then it would be bad to return 20 as the answer if
	an error occurred on the final (third) fetch.
	Instead we would return 0 and leave the user to examine the .log file
	to gain more information.
	"""

	global con

	cur = connect_get_cursor(database_path)
	if cur is None:
		return 0

	""" Having a cursor object does not guarantee that the file was an sqlite
	database. A plain text file get a cursor, but fail on cur.execute() """

	sqlstring_table_count = "SELECT name from sqlite_master WHERE type='table';"
	try:
		cur.execute(sqlstring_table_count)
	except db3.ProgrammingError, e:
		logpy.error('Sqlite execute() ProgrammingError occurred: %s', e)
		return 0
	except db3.Error, e:
		logpy.error('Sqlite execute() get table names error %s: %s',
			    database_path, e.args[0])
		return 0
	except:
		logpy.error('Sqlite error during .execute() get table names.')
		return 0
	""" finally:
		con.close() """
	""" row = cur.fetchone()
	table_count = row[0] """
	try:
		rows = cur.fetchall()
		cur.close()
	except db3.Error, e:
		logpy.error('Sqlite error during tables fetch/close: %s', e)
		return 0
	except:
		logpy.error('Sqlite error during tables fetch & close.')
		return 0
	""" finally:
		con.close() """
	""" table_count = len(rows) """
	curinner = con.cursor()
	rowcount = 0
	for row in rows:
		tname = str(row[0])
		""" str() above might fail, if you really do have unicode
		characters in your table names """
		tname = tname.translate(None,punctuation.translate(None,'_-'))
		""" Nothing bad in tname gets through to query (underscore & dash ok) """
		try:
			""" curinner.execute("SELECT COUNT(*) FROM ?", (tname,))
			Tables as targets for parameter substitution supported? """
			sqlstring_rowcount = "SELECT COUNT(*) FROM '{0!s}';".format(tname)
			curinner.execute(sqlstring_rowcount)
		except db3.OperationalError, e:
			err_str = 'Sqlite execute() OperationalError occurred: {0}'.format(e)
			print err_str
			""" logpy.error('Sqlite execute() error occurred:', e.args[0]) """
			logpy.error(err_str)
			return 0
		except db3.ProgrammingError, e:
			err_str = 'Sqlite execute() ProgrammingError occurred: {0}.format(e)'
			""" logpy.error('Sqlite execute() ProgrammingError occurred:', e) """
			logpy.error(err_str)
			return 0
		except db3.Error, e:
			err_str = 'Sqlite execute() error occurred: {0}'.format(e)
			print err_str
			""" logpy.error('Sqlite execute() error occurred:', e.args[0]) """
			logpy.error(err_str)
			return 0
		except:
			logpy.info('Sqlite fetch from table %s is skipped.', tname)
			return 0
		""" finally:
			con.close() """


		try:
			rowinner = curinner.fetchone()
		except db3.Error, e:
			logpy.error('Sqlite error during rowcount fetch/close:', e.args[0])
			return 0
		except:
			logpy.error('Sqlite error during row count fetch & close.')
			return 0
		""" finally:
			con.close() """

		rowcount += rowinner[0]

	curinner.close()
	con.close()

	return rowcount


logging.basicConfig(level=logging.INFO)
logpy = logging.getLogger('rowcount_of_sqlite')
import getpass
uname = str(getpass.getuser())
uname2 = uname.translate(None,punctuation)
if (uname == 'root'):
	logpath = '/root'
elif (uname == uname2):
	logpath = '{1:<1}{0}{1:<1}{2}'.format('home','/',uname)
else:
	logpath = '/tmp'
""" logpath = '/tmp' """

lh = logging.FileHandler('{0}{1:<1}rowcount_of_sqlite.log'.format(logpath,'/'))
""" Inherit rather than doing lh.setLevel(logging.INFO) """
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
lh.setFormatter(formatter)
logpy.addHandler(lh)
""" Comment out the logpy.propagate line if you want console output also. """
logpy.propagate = False

def process_from_stdin():
	import fileinput
	for line in fileinput.input():
		line_stripped = line.strip()
		rowcount_returned = 0
		if not isfile_and_positive_size(line_stripped):
			rowcount_report(0,line_stripped)
			continue
		try:
			open(line_stripped,'r')
		except IOError as e:
			logpy.error('Error opening %s', line_stripped)
		except:
			logpy.error('open error')
		else:
			rowcount_returned = compound_rowcount(line_stripped)
			rowcount_report(rowcount_returned,line_stripped)

if (len(argv) < 2):
	process_from_stdin()
	exit(0)

for arg_index in range(1,len(argv)):
	command_line_arg = argv[arg_index]
	rowcount_returned = 0
	if not isfile_and_positive_size(command_line_arg):
		rowcount_report(0,command_line_arg)
		continue
	try:
		open(command_line_arg,'r')
	except IOError as e:
		logpy.error('Error opening %s', command_line_arg)
	except:
		logpy.error('open error')
	else:
		rowcount_returned = compound_rowcount(command_line_arg)
        rowcount_report(rowcount_returned,command_line_arg)
