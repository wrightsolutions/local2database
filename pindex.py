#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) 2019 Gary Wright http://www.wrightsolutions.co.uk/contact
# Distributed under the BSD '3 clause' software license, see the accompanying
# file COPYING or https://opensource.org/licenses/BSD-3-Clause

from sys import argv,exit
ADDINDEX_HEAD="""<?php

use Phinx\Migration\AbstractMigration;

class MyNewMigration extends AbstractMigration
{
    public function change()
    {
        $table = $this->table('address');
"""

ADDINDEX_TAIL="""            ->create();
    }
}

?>
"""

# ->addIndex(['email'], ['unique' => true, 'name' => 'idx_users_email'])
# ->addIndex([ 'code' ], [ 'name' => 'code_UNIQUE', 'unique' => true ])
# ->addIndex(['username', 'email'], ['unique' => true, 'name' => 'idx_username_email'])
# ->addIndex('email', ['type' => 'fulltext'])
# $table = $this->table('users', ['engine' => 'MyISAM']);





if __name__ == '__main__':

	program_binary = argv[0].strip()
	# First arg is expected to be an alphanumeric string
	field1 = argv[1]
	field2 = None
	field3 = None
	if len(argv) > 2:
		field2 = argv[2]
		if len(argv) > 3:
			field3 = argv[3]
	prefix='$table->addIndex'
	middle = None

	middle_template = """
(['{0}', '{1}', {2}], ['unique' => true, 'name' => 'idx_{0}_{1}_{2}'])
""".strip()

	if field3 is None:
		middle_template = """
(['{0}', '{1}'], ['unique' => true, 'name' => 'idx_{0}_{1}'])
""".strip()
		if field2 is None:
			middle = "(['{0}'], ['unique' => true, 'name' => 'idx_{0}'])".format(field1)
		else:
			middle = middle_template.format(field1,field2)
	else:
			middle = middle_template.format(field1,field2,field3)
	if middle is not None:
		print(ADDINDEX_HEAD)
		print("            {0}{1};".format(prefix,middle))
		print(ADDINDEX_TAIL)

	#exit(151)

