#!/usr/bin/env python
#   Copyright 2015 Gary Wright http://www.wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
#   On Debian systems, the complete text of the Apache license can be found in
#   /usr/share/common-licenses/Apache-2.0

# For the given csv file, create a table in database 'employees'
# based on --coltypes supplied on command line or guesswork

# 
# python ./csv2pgtemp.py  --coltypes date var80 text text text text text text var80 var80 var80

# employees=# \d temp11
#           Table "public.temp11"
# Column |         Type          | Modifiers 
#--------+-----------------------+-----------
# col1   | date                  | 
# col2   | character varying(80) | 
# col3   | text                  | 
# col4   | text                  | 
# col5   | text                  | 
# col6   | text                  | 
# col7   | text                  | 
# col8   | text                  | 
# col9   | character varying(80) | 
# col10  | character varying(80) | 
# col11  | character varying(80) |

# Ensure quoting is Postgresql friendly single quote as 
# double quotes are generally used to quote system identifiers
# such as field names, table names, and so on

from __future__ import print_function
import argparse
import re
from os import path as ospath
from os import stat
import csv

from string import printable

try:
    import psycopg2 as psy
except ImportError:
    import psycopg as psy

DBTEMP='employees'
CONN_USER='postgres'
conn = None
try:
    conn = psy.connect("dbname={0} user={1}".format(DBTEMP,CONN_USER))
except psy.OperationalError as e:
    print("Connection to database failed:\n\t{0}".format(e.args[0]))

set_printable = set(printable)

VERSION='0.1'
DESCRIPTION=""" From given csv file load the data into
a table in database specified internally as DBTEMP.

A table create will be generated ideally defined based on the --coltypes
you gave when running the program such as --coltypes followed by 3 args
smallint varchar80 int4

If you invoke with --coltypes text
... then all columns will be created as 'text'
"""

#READER_QUOTECHAR=chr(34)
READER_QUOTECHAR=None
WRITER_QUOTECHAR=chr(34)


#QUOTING_OUTPUT = csv.QUOTE_MINIMAL
QUOTING_OUTPUT = csv.QUOTE_ALL
"""
Above enable whichever of csv.QUOTE_MINIMAL or csv.QUOTE_ALL 
you prefer. csv.QUOTE_ALL is going to force quoting regardless
of the need and is helpful when the data input target
is expecting every field to be quoted.
csv.QUOTE_NONE may result in csv.Error: need to escape
If csv.QUOTE_ALL seems too much then try csv.QUOTE_MINIMAL
"""

def isfile_and_positive_size(target_path_given):
    target_path = target_path_given.strip()
    if not ospath.isfile(target_path):
        return False
    if not stat(target_path).st_size:
        return False
    return True


def linesample(filepath):
    """ Return a list of items sampled from the given csv file
    Content is not important at this stage as we are just going
    to use len() on sampled_list in the caller, but features
    are there to be developed as required.
    """

    sampled_list = []

    filepath_expanded = ospath.expanduser(filepath)

    if not isfile_and_positive_size(filepath):
        return sampled_list

    with open(filepath_expanded,'r') as csvfile1:
        reader1 = csv.reader(csvfile1,delimiter=chr(44),quotechar=READER_QUOTECHAR)
        header_as_list = reader1.next()
        fieldnames_list = []
        for fieldname in header_as_list:
            fieldnames_list.append(fieldname.strip('\'"'))
        dataline = reader1.next()
        """ For now just have sampled_list populated from fieldnames_list
        eventually you might add some logic to compare the two counts of
        fields and if matching have sampled_list be the fields from dataline,
        but for this implementation something basic will do
        """
        sampled_list = fieldnames_list

    return sampled_list


def create_temptable(colcount,coltypes,colnames=None,tablename=None):
    conn = None
    try:
        conn = psy.connect("dbname={0} user={1}".format(DBTEMP,CONN_USER))
    except psy.OperationalError as e:
        print("Connection to database failed:\n\t{0}".format(e.args[0]))

    if coltypes is None or conn is None:
        return False

    columns_alltext = ''
    for idx in range(1,colcount):
        columns_alltext += "col{0} text,\n".format(idx)
    if colcount < 2:
        # Just define it all in one go here
        columns_alltext = "col1 text\n"
    else:
        # Add a tail that follows same form but commaless
        columns_alltext += "col{0} text\n".format(colcount)

    colnames_list = []
    if colcount == len(coltypes):
        # We have a full and complete column specification so use that instead
        columns = ''
        for idx in range(0,colcount):
            idx1 = idx+1
            """ Above a convenience 1 indexed """
            colname = "col{0}".format(idx1)
            if coltypes[idx] == 'date':
                coltype = 'date'
            elif coltypes[idx] == 'int4':
                coltype = 'int4'
            elif coltypes[idx] == 'integer':
                coltype = 'int4'
            elif coltypes[idx] == 'smallint':
                coltype = 'smallint'
            elif coltypes[idx] == 'text':
                coltype = 'text'
            elif coltypes[idx] == 'varchar80':
                coltype = 'varchar(80)'
            elif coltypes[idx] == 'var80':
                coltype = 'varchar(80)'
            else:
                coltype = 'text'
            try:
                cname = colnames[idx]
                if set(cname) in set_printable:
                    colname = cname
            except:
                pass
            colnames_list.append(colname)
            if len(columns) > 2:
                columns += ",\ncol{0} {1}".format(idx1,coltype)
            else:
                columns += "col{0} {1}".format(idx1,coltype)
        #print(columns)
    else:
        # Use the generic alltext when populating variable create_table
        columns = columns_alltext

    create_table = """
CREATE TABLE temp{0} (
{1}
);
""".format(colcount,columns)

    print("Table create will be as follows:")
    print(create_table)
    print("...proceeding...")

    return colnames_list


def copy_command(colnames_list,sampled_list=[]):
    colnames_length = len(colnames_list)
    inner_content = colnames_list[0]
    for idx in range(1,colnames_length):
        inner_content += "\n,{0}".format(colnames_list[idx])
    copy_string = """COPY temp{0}(
{1})
FROM '/tmp/temp.csv'
WITH DELIMITER ','
CSV HEADER""".format(colnames_length,inner_content)

    return (inner_content,copy_string)



if __name__ == "__main__":

    par = argparse.ArgumentParser(description=DESCRIPTION)
    par.add_argument('-v', '--version', help='Show version',
                     action='store_true')
    par.add_argument('--coltypes', nargs='+',
                     required=True, dest='coltypes_given')

    args = par.parse_args()

    exit_rc = 0
    if args.version:
        print(VERSION)
        exit(exit_rc)

    FILEPATH='/tmp/temp.csv'
    """ Fix the input file filepath for now and
    if you enance it later then can implement another add_argument
    """
    sampled_list = linesample(FILEPATH)
    if len(sampled_list) > 0:
        print(args.coltypes_given)
        colnames_list = create_temptable(len(sampled_list),args.coltypes_given)
        inner_content,copy_string = copy_command(colnames_list,sampled_list)
        print(copy_string)
        print("   ;   ")
        print("INSERT INTO realtable1 (...) ")
        print("SELECT {0} FROM temp{1};".format(inner_content,len(colnames_list)))
    else:
        print("Sampling of file {0} did not complete as expected".format(FILEPATH))
        exit_rc = 110

    exit(exit_rc)

