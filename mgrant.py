#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) 2018 Gary Wright http://www.wrightsolutions.co.uk/contact
# Distributed under the BSD '3 clause' software license, see the accompanying
# file COPYING or https://opensource.org/licenses/BSD-3-Clause
# 
# python ./mgrant.py mydb myuser imagebin '172.16.0.0'
# python ./mgrant.py mydb myuser imagebin '172.16.0.%'
# Above is a /24 subnet qualified grant
# python ./mgrant.py mydb myuser imagebin '172.16.0.0/255.255.224.0'
# Above is a /19 subnet qualified grant
# python ./mgrant.py mydb myuser dropper '127.0.0.1'
# python ./mgrant.py mydb myuser admin --percent
# python ./mgrant.py mydb myuser drupal --percent

from __future__ import print_function
import argparse
import random
from string import printable,ascii_lowercase,ascii_uppercase
from string import digits as string_digits

SET_PRINTABLE = set(printable)
PERC=chr(37)
COMMA=chr(44)
ATSYM=chr(64)	# chr() code for @

VERSION='0.1'
DESCRIPTION=""" Generate a suitable grant statement for input
into MariaDB or MySQL based on supplied arguments

If you invoke with --percent 
... then all grants will be of the @'%' type
"""

def selective_chars(length=24,selection_set=set('23456789abcdefghjkmnpqrstwxyz')):
    if selection_set is None:
        selection_set = set(string_digits+ascii_lowercase+ascii_uppercase)
    elif len(selection_set) < 10:
        selection_set=set('23456789abcdefghjkmnpqrstwxyzABCDEFGHJKMNPQRSTWXYZ')
    else:
        pass
    pass_as_list = [ random.choice(tuple(selection_set)) for n in range(length) ]
    chars = ''.join(pass_as_list)
    assert length == len(chars)
    return chars


if __name__ == "__main__":

    par = argparse.ArgumentParser(description=DESCRIPTION)
    par.add_argument('-v', '--version', help='Show version',
                     action='store_true')
    par.add_argument('dbname', type=str,
                     help='Database name for which privileges are to be granted.')
    par.add_argument('user', type=str,
                     help='username for the account (and grant)')
    par.add_argument('type', type=str,
                     help='The [broad] type of grant you want to give [imagebin,dropper,admin,drupal]')
    par.add_argument('hostmask', type=str, default='%',nargs='?',
                     help='Host mask for the user creation (and grant)')
    par.add_argument('-p', '--percent', help='Hostmask is very wide (%)',
                     action='store_true', default=False)

    args = par.parse_args()

    exit_rc = 0
    if args.version:
        print(VERSION)
        exit(exit_rc)

    dbname = None
    if set(args.dbname).issubset(SET_PRINTABLE):
        dbname = args.dbname.strip()
    else:
        exit_rc = 111
        exit(exit_rc)

    user = None
    if set(args.user).issubset(SET_PRINTABLE):
        user = args.user.strip()
    else:
        exit_rc = 112
        exit(exit_rc)

    type = None
    if set(args.type).issubset(SET_PRINTABLE):
        type = args.type.strip()
    else:
        exit_rc = 113
        exit(exit_rc)

    hostmask = None
    if set(args.hostmask).issubset(SET_PRINTABLE):
        hostmask = args.hostmask.strip()

    grantee = "'{0}'@".format(user)
    if args.percent is True:
        grantee = "{0}'%'".format(grantee)
        print(PERC,PERC,PERC)
    elif hostmask.strip() == '%':
        grantee = "{0}'%'".format(grantee)
    else:
        grantee = "{0}'{1}'".format(grantee,hostmask)
    
    CREATEUSER_TEMPLATE="CREATE USER {0} IDENTIFIED BY '{1}';"
    userpass = selective_chars(24)
    createuser = CREATEUSER_TEMPLATE.format(grantee,userpass)
    GLIST4 = 'SELECT, INSERT, UPDATE, DELETE'
    GLIST5 = 'SELECT, INSERT, UPDATE, DELETE, CREATE TEMPORARY TABLES'
    GLIST_ADMINLIKE_LIST="""CREATE, DROP, INDEX, ALTER, REFERENCES,
CREATE TEMPORARY TABLES, CREATE VIEW, SHOW VIEW
""".strip().split(COMMA)
    GLIST_ADMINLIKE=', '.join([g.strip() for g in GLIST_ADMINLIKE_LIST])
    GLIST_SUPERLIKE='CREATE_ROLE, DROP_ROLE'
    GLIST_UPLOADER='FILE'
    GLIST_GRANTOR='GRANT OPTION'
    GLIST_EXECUTELOCKER='TRIGGER, EXECUTE, LOCK TABLES, PROCESS'
    CREATEUSER_TEMPLATE="CREATE USER {0} IDENTIFIED BY '{1}';"
    user = CREATEUSER_TEMPLATE.format(grantee,userpass)
    grants = GLIST4
    if type.startswith('admin2'):
         grants = "{0},{1},{2}".format(GLIST4,GLIST_ADMINLIKE,GLIST_EXECUTELOCKER)
    elif type.startswith('admin'):
         grants = "{0},{1},{2},{3}".format(GLIST4,GLIST_ADMINLIKE,GLIST_EXECUTELOCKER,GLIST_SUPERLIKE)
    elif type.startswith('drupal'):
         grants = "{0}, {1}".format(GLIST4,GLIST_UPLOADER)
    elif type.startswith('im'):
         grants = GLIST5
    usergrant = ''
    if ATSYM in createuser:
        usergrant = "GRANT {0} ON {1}.* TO {2};".format(grants,dbname,grantee)
    print(createuser+usergrant)
    exit(exit_rc)

