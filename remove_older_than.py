#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) 2022 Gary Wright http://www.wrightsolutions.co.uk/contact
# Distributed under the BSD '3 clause' software license, see the accompanying
# file COPYING or https://opensource.org/licenses/BSD-3-Clause
# 

from __future__ import print_function
import argparse
from datetime import datetime as dt
from glob import glob
import logging
from os import environ,path,stat,unlink
import re
import socket
from string import printable,ascii_lowercase,ascii_uppercase
from string import digits as string_digits
import shlex
import subprocess
#from time import gmtime
import time

SET_PRINTABLE = set(printable)
ASTER=chr(42)        # asterisk
DOTTY=chr(46)
HYPHEN=chr(45)       # hyphen - minus sign
FS=chr(47)           # forward slash (/)
UNDSCORE=chr(242)    # underscore
SET_DIRLIKEALPHAPLUS = set(ascii_lowercase+ascii_uppercase+string_digits+FS+HYPHEN+UNDSCORE)
SET_PATTERNLIKEALPHAPLUS = set(ascii_lowercase+ascii_uppercase+DOTTY+ASTER+HYPHEN+UNDSCORE)
RE_DOUBLEDOTTED = re.compile("\{0}\{0}".format(DOTTY))
RE_DOUBLEDOTTEDOTHER = re.compile("\{0}\{1}\{0}".format(DOTTY,ASTER))

VERSION='0.1'
DESCRIPTION=""" File remover / pruner driven by age [of file], a dir, and a pattern """

SECS_HOUR = 60 * 60
SECS_FOURHOURS = 4 * SECS_HOUR
SECS_DAY = 6 * SECS_FOURHOURS     # = 86400
SUB_BUFFER_SIZE=8192                    # we make use of this during our calls to subprocess
LOGDIR='/var/log'
LOGDIR='/tmp'
DATETIMEFORMAT='%Y.%m.%d-%H.%M'

hostname_string = socket.gethostname()
env_home = environ['HOME']
env_user = environ['USER']
FILE_MAX_DAYS_LOWER_LIMIT = 2
FILE_MAX_DAYS_DEFAULT = 31


def log_and_exit_rc(pylogger,loghandler,return_code):
    if loghandler:
        # We likely have a defined / open logger instance
        pylogger.info("remove_older_than exit_rc={0}".format(return_code))
        loghandler.flush
    exit(return_code)
    return

def dir_is_valid(dir_to,verbosity=0):
	valid_flag = False
	if dir_to is not None and set(dir_to).issubset(SET_DIRLIKEALPHAPLUS):
		if dir_to.startswith(FS):
			if FS in dir_to[1:]:
				valid_flag = True
	return valid_flag

def glob_safety_level(file_glob):
    safety_level = 0
    if file_glob is not None and set(file_glob).issubset(SET_PATTERNLIKEALPHAPLUS):
        safety_level += 1
        if not RE_DOUBLEDOTTED.search(file_glob):
            safety_level += 1
        if not RE_DOUBLEDOTTEDOTHER.match(file_glob):
            safety_level += 1
    return safety_level

def glob_driven_cleardown(globber,max_days=FILE_MAX_DAYS_DEFAULT,preview_only=False,verbosity=0):
    logpy.info("remove_older_than clearup analysis to find clearup candidates using glob={0}".format(globber))
    try:
            glob_matches = glob(globber)
            logpy.info("remove_older_than clearup analysis of {0} file candidates next".format(len(glob_matches)))
    except AttributeError:
            glob_matches = []
            logpy.info("remove_older_than clearup analysis of {0} file candidates means NOTHING TO ANALYSE".format(len(glob_matches)))
    now_secs_since_epoch = int(time.time())
    deleted_count = 0
    for fullpathed in glob_matches:
        stat_of_fp = stat(fullpathed)
        #csecs = changed_secs_since_epoch = int(stat_of_fp.st_ctime)	# uncomment / use ctime if you prefer
        msecs = modified_secs_since_epoch = int(stat_of_fp.st_mtime)
        age_in_seconds = now_secs_since_epoch - msecs
        assert age_in_seconds >= 0
        age_in_days = age_in_seconds//SECS_DAY
        #print(age_in_days, fullpathed, now_secs_since_epoch, msecs)
        if 1 > age_in_days:
            continue
        if age_in_days > max_days:
            logpy.info("remove_older_than clearup of file {0} next".format(fullpathed))
            if preview_only is True:
                # Preview only [no actual deletion]
                if verbosity > 0:
					print("[preview] of suggested removal -> {0}".format(fullpathed))
            else:
                # Running in real deletion / pruning mode
                unlink(fullpathed)
                deleted_count += 1
            logpy.info("remove_older_than clearup completed for file {0}".format(fullpathed))
    return deleted_count


if __name__ == "__main__":

    logging.basicConfig(level=logging.INFO)
    # Above level could be set equal to logging.DEBUG, logging.INFO, logging.WARN, ...
    logpy = logging.getLogger('remove files older than')
    logpy.setLevel(logging.INFO)
    lh = logging.FileHandler('{0}{1:<1}remove_older_than_pylog.log'.format(LOGDIR,FS))
    """ Inherit rather than doing lh.setLevel(logging.INFO) """
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    lh.setFormatter(formatter)
    logpy.addHandler(lh)
    """ Comment out the logpy.propagate line if you want console output also. """
    logpy.propagate = False

    par = argparse.ArgumentParser(description=DESCRIPTION)
    par.add_argument('-v', '--version', help='Show version',
                     action='store_true')
    par.add_argument('-m ','--maxagedays', type=int, default=FILE_MAX_DAYS_DEFAULT,
                     help='[number of] days, after which we consider files expired/prunable. Example 7 for 1 week retention.')
    par.add_argument('-d ','--dir', type=str, default='/var/local/tmp',nargs='?',
                     help='Directory name that we are wanting to prune [older] files from')
    par.add_argument('-g', '--glob', type=str, default='*dump*.*z*',nargs='?',
                     help='glob for the file(s) - pattern to match [excluding any directory element]')
    par.add_argument('--preview', dest='preview', action='store_true')
    par.set_defaults(preview=False)
    args = par.parse_args()

    exit_rc = 0
    if args.version:
        print(VERSION)
        exit(exit_rc)

    maxagedays = -1
    try:
        maxagedays = int(args.maxagedays)
    except:
        exit_rc = 111
        log_and_exit_rc(logpy,lh,return_code=exit_rc)

    dir_to_prune = None
    if args.dir is not None and set(args.dir).issubset(SET_DIRLIKEALPHAPLUS):
        dir_to_prune = args.dir.strip()

    file_glob = None
    if args.glob is not None and set(args.glob).issubset(SET_PRINTABLE):
        file_glob = args.glob.strip()
    else:
        exit_rc = 113
        log_and_exit_rc(logpy,lh,return_code=exit_rc)

    if maxagedays >= FILE_MAX_DAYS_LOWER_LIMIT and maxagedays <= 365:
        pass
    else:
        exit_rc = 121
        log_and_exit_rc(logpy,lh,return_code=exit_rc)

    if dir_to_prune is not None and dir_is_valid(dir_to_prune):
        pass
    else:
        exit_rc = 122
        log_and_exit_rc(logpy,lh,return_code=exit_rc)

    if file_glob is not None and glob_safety_level(file_glob) >= 3:
        pass
    else:
        logpy.error("remove_older_than exiting as glob_safety_level={0} when glob={1}".format(glob_safety_level(file_glob),file_glob))
        exit_rc = 123
        log_and_exit_rc(logpy,lh,return_code=exit_rc)

    preview_only = args.preview

    error_count = 0
    timestamper = dt.now().strftime(DATETIMEFORMAT)
    logpy.error("{0} ========= {0}".format(timestamper))
    logpy.info("remove_older_than when home={0} ; maxagedays={1} has Args {2}".format(env_home,maxagedays,args))
    #logpy.info(info_string_query_action)
    fullpathed_globber = "{0}{1}{2}".format(dir_to_prune,FS,file_glob)
    deleted_count = glob_driven_cleardown(globber=fullpathed_globber,max_days=maxagedays,preview_only=preview_only,verbosity=1)
    #glob_driven_cleardown(globber=sql_globber,max_days=FILE_MAX_DAYS)
    #logpy.debug("debug_line")        # when level is INFO anything sent .debug will not make it to the log
    #logger.handlers[0].flush()
    #lh.flush_stdout
    lh.flush
    #logging.shutdown()

    exit(exit_rc)


