#!/usr/bin/env python
#   Copyright 2012 Gary Wright http://identi.ca/wrightsolutions
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

from __future__ import print_function  # We like to be forward thinking so we place this at first line after hashbang
import codecs
pythonstring = "\xc2\xb2\xc2\xb3\xc2\xb9\xc2\xbc\xc2\xbd\xc2\xbe\xc2\xbf\x0a"
#encoded1string = pythonstring.encode("iso-8859-1")
converted1string = unicode(pythonstring, "iso-8859-1")  # str(latin1string, utf8) in python3
utf8string="\xc2\xb2\xc2\xb3\xc2\xb9\xc2\xbc\xc2\xbd\xc2\xbe\xc2\xbf\x0a"
converted8string = unicode(pythonstring, "utf8")  # str(latin1string, utf8) in python3
encoded1string = converted8string.encode("latin-1")
print(len(pythonstring),":")
print(pythonstring)
print(len(converted1string),":")
print(converted1string)
print(len(converted8string),":")
print(converted8string)
print(len(encoded1string),":")  # cannot be output with Codec.write(). You should output converted8string using Codec.write()
print(encoded1string)
converted1file = codecs.open("/tmp/unicodeFromLatin1.out.latin1","w","latin-1")
converted1file.write(converted8string)
#converted1file.write(encoded1string)  # attempting to Codec.write(encoded1string) will likely get you some error message complaining about ascii
converted1file.close()

