#!/usr/bin/env python
#   Copyright 2014 Gary Wright http://www.wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
#   On Debian systems, the complete text of the Apache license can be found in
#   /usr/share/common-licenses/Apache-2.0

# Example asking to verify that table 'page_page' in example.db sqlite database
# has a minimum of 10 rows:
# python ./sqlite_table_exists_minimum_count.py example.db 'page_page' 10

# Program exits with zero on success or a code between 101 and 125 on failure.
# Return code 110 indicates that database was consulted, table was found,
# but minimum number of rows target was not achieved.

from sys import argv,exit
import os.path
from os import stat
import sqlite3 as db3
from string import punctuation


def isfile_and_positive_size(target_path_given):
	target_path = target_path_given.strip()
	if not os.path.isfile(target_path):
		return False
	if not os.stat(target_path).st_size:
		return False
	return True


def connect_get_cursor(database_path):
	global con
	cur = None
	try:
		""" Ideally the next would be READONLY but we are using Python
		standard DB-API so fit with what it offers. """
		con = db3.connect(database_path,timeout=2)		
	except db3.Error, e:
		return None
	except:
		return None
	else:
		con.isolation_level = None		
		cur = con.cursor()
	return cur


def table_minimum_reached(database_path,table_target,minimum_count=1):
	""" Return code 110 indicates that database was consulted, table was found,
	but minimum number of rows target was not achieved.
	"""

	global con

	cur = connect_get_cursor(database_path)
	if cur is None:
		return 101

	""" Having a cursor object does not guarantee that the file was an sqlite
	database. A plain text file get a cursor, but fail on cur.execute() """

	sqlstring_table_count = "SELECT name from sqlite_master WHERE type='table';"
	try:
		cur.execute(sqlstring_table_count)
	except db3.ProgrammingError, e:
		return 102
	except db3.Error, e:
		return 102
	except:
		return 102
	""" finally:
		con.close() """
	""" row = cur.fetchone()
	table_count = row[0] """
	try:
		rows = cur.fetchall()
		cur.close()
	except db3.Error, e:
		return 103
	except:
		return 103
	""" finally:
		con.close() """
	""" table_count = len(rows) """
	minimum_rc = 104
	table_list = []
	for row in rows:
		tname = str(row[0])
		""" str() above might fail, if you really do have unicode
		characters in your table names """
		try:
			tname = tname.translate(None,punctuation.translate(None,'_-'))
		except:
			tname = None
		""" Nothing bad in tname gets through to query (underscore & dash ok) """
		if tname is None:
			continue
		table_list.append(tname)

	minimum_rc = 105
	rowcount = 0
	if table_target in table_list:
		minimum_rc = 110
		curinner = con.cursor()
		try:
			""" curinner.execute("SELECT COUNT(*) FROM ?", (tname,))
			Tables as targets for parameter substitution supported? """
			sqlstring_rowcount = "SELECT COUNT(*) FROM '{0!s}';".format(table_target)
			#print sqlstring_rowcount
			curinner.execute(sqlstring_rowcount)
		except db3.OperationalError, e:
			err_str = 'Sqlite execute() OperationalError occurred: {0}'.format(e)
			print err_str
			return 115
		except db3.ProgrammingError, e:
			err_str = 'Sqlite execute() ProgrammingError occurred: {0}.format(e)'
			return 115
		except db3.Error, e:
			err_str = 'Sqlite execute() error occurred: {0}'.format(e)
			print err_str
			return 115
		except:
			return 115
		""" finally:
			con.close() """

		try:
			rowinner = curinner.fetchone()
			#print rowinner
		except db3.Error, e:
			return 120
		except:
			return 120
		""" finally:
			con.close() """

		rowcount = rowinner[0]

		curinner.close()
	con.close()

	if rowcount >= minimum_count:
		minimum_rc = 0

	return minimum_rc


if __name__ == "__main__":
    if len(argv) < 2:
	    exit(101)
    dbpath = argv[1]
    table = argv[2]
    mincount = 1
    if len(argv) > 3:
	    mincount = int(argv[3])
    reached_rc = table_minimum_reached(database_path=dbpath,
				       table_target=table,minimum_count=mincount)
    if reached_rc == 0:
	    exit_rc = 0
    else:
	    exit_rc = 125
	    try:
		    exit_rc = int(reached_rc)
	    except:
		    exit_rc = 125
	    exit(exit_rc)
    exit(exit_rc)
